﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Risk.Tables
{
    //anotaciones
    [Table("Players")]
	class Player : IComparable<Player>
	{
        [Key]
        [Column("id")]
		public int Id { get; set; }

        [Column("order")]
        [Required]
        public int Order { get; set; }

        [Column("name")]
        [Required]
        public string Name { get; set; }

        [Column("wins")]
        [Required]
        public int Wins { get; set; }

        [Required]
        public SortedSet<Region> Regions { get; set; }

        [Required]
        public SortedSet<Continent> Continents { get; set; }

		public Player()
		{
			Regions = new SortedSet<Region>();
			Continents = new SortedSet<Continent>();
		}

        public int CompareTo(Player other)
        {
            return Order - other.Order;
        }
    }

    //inferencia y fluent api (relacion con continente)
	class Region : IComparable<Region>
    {
		public int Id { get; set; }
		public string Name { get; set; }
        public Continent Continent { get; set; }
        public int ContinentId { get; set; }
		public Player Owner { get; set; }
		public int Troops { get; set; }
		public SortedSet<Region> Neighbours { get; set; }
        public SortedSet<Region> ReverseNeighbours { get; set; }
		public Region()
		{
			Neighbours = new SortedSet<Region>();
		}

        public int CompareTo(Region other)
        {
            return Name.CompareTo(other.Name);
        }
    }

    //inferencia y fluent api (relacion con regiones)
    class Continent : IComparable<Continent>
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int Bonus { get; set; }
        public Player Owner { get; set; }
        public int? OwnerId { get; set; }
		public SortedSet<Region> Regions { get; set; }

		public Continent()
		{
			Regions = new SortedSet<Region>();
		}

        public int CompareTo(Continent other)
        {
            return Name.CompareTo(other.Name);
        }
    }
}

