﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Risk.Contexts;
using Risk.Tables;
using Risk;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebService
{
	//localhost:port/api/Controlador/(acciones y parametros)
	[Route("api/[controller]")]
    [ApiController]
    public class Controlador : Controller
    {
        
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpGet("view")]
        public string ViewAll()
        {
            RiskContext ctx = RiskContext.getInstance();

            string result = "";
            List<Region> regions = ctx.Regions.ToList();
            foreach (Region r in regions)
            {
                result += r.Name + " -> " + r.Owner.Name + " " + r.Troops + "\n";
            }
            result += "\n\n";

            List<Player> players = ctx.Players.ToList();
            foreach(Player p in players)
            {
                result += p.Name + " --> " + p.Continents.Count() + " continentes";
            }
            return result;
        }
        
        [HttpGet("{jugador}/attack")]
        public string Attack(int jugador, int r1, int r2)
        {
            RiskContext ctx = RiskContext.getInstance();
            Player p = ctx.Players.Find(jugador);
            Region reg1 = ctx.Regions.Find(r1);
            Region reg2 = ctx.Regions.Find(r2);
            if(ctx.turnPlayer == p && reg1.Owner == p)
            {
                Attack(reg1, reg2);
            }
            ctx.SaveChanges();
            return "Ataque";
        }

        [HttpGet("{jugador}/reassign")]
        public string Reassign(int jugador, int r1, int r2, int mov)
        {
            RiskContext ctx = RiskContext.getInstance();
            Player p = ctx.Players.Find(jugador);
            Region reg1 = ctx.Regions.Find(r1);
            Region reg2 = ctx.Regions.Find(r2);

            if(ctx.turnPlayer == p && reg1.Owner == p && reg2.Owner == p)
            {
                reg1.Troops -= mov;
                reg2.Troops += mov;
                ctx.SiguienteTurno();
            }
            ctx.SaveChanges();
            return "Reasignadas tropas";
        }

        private void Attack(Region attacking, Region attacked)
        {
            int attackingTroops = attacking.Troops > 3 ? 3 : attacking.Troops - 1;
            int defendingTroops = attacked.Troops > 2 ? 2 : attacked.Troops;

            if (attackingTroops == 0) { return; }

            DiceRoll(attackingTroops, defendingTroops, out int attackerLosses, out int defenderLosses);

            attacking.Troops -= attackerLosses;
            attacked.Troops -= defenderLosses;

            bool conquest = attacked.Troops == 0;

            if (conquest)
            {
                attacked.Troops = attacking.Troops - 1;
                attacking.Troops = 1;
                attacked.Owner = attacking.Owner;
            }
        }

        public static void DiceRoll(int natk, int ndef, out int lossatk, out int lossdef)
        {
            int[] atk = new int[natk];
            int[] def = new int[ndef];
            lossatk = 0;
            lossdef = 0;

            Random r = new Random();
            for (int i = 0; i < natk; i++)
            {
                atk[i] = r.Next(1, 7);
            }
            for (int i = 0; i < ndef; i++)
            {
                def[i] = r.Next(1, 7);
            }
            Array.Sort(atk);
            Array.Reverse(atk);
            Array.Sort(def);
            Array.Reverse(def);

            for (int i = 0; i < ndef && i < natk; i++)
            {
                if (atk[i] > def[i]) lossdef++;
                else lossatk++;
            }

        }
    }
}
