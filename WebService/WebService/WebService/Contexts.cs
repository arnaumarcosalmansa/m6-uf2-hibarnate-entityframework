﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Risk.Tables;

namespace Risk.Contexts
{
    class RiskContext : DbContext
    {
        private static RiskContext ctx;

        public Player turnPlayer;

        public void SiguienteTurno()
        {
            SortedSet<Player> players = new SortedSet<Player>(this.Players);

			Player auxPlayer = turnPlayer;
			//no sé si funciona
            bool end = false;
            foreach(Player p in players)
            {
                if(end)
                {
                    turnPlayer = p;
					break;
                }

                if(turnPlayer == p)
                {
                    end = true;
                }
            }

			if(auxPlayer == turnPlayer)
			{
				turnPlayer = players.First();
			}
        }

        public static RiskContext getInstance()
        {
            if(ctx == null)
            {
                ctx = new RiskContext();
            }
            return ctx;
        }

        public RiskContext() : base("Risk")
        {
            Console.WriteLine("Creando contexto... puede tardar un poco.");
            Setup();
            //asegurarse del orden
            turnPlayer = new SortedSet<Player>(this.Players).First();
        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Continent> Continents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists); //update, pero crea la DB!
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RiskContext>());  //aquest seria un metode update pero si canvies el model (els POCOS) reinicia la DB
            Database.SetInitializer(new DropCreateDatabaseAlways<RiskContext>());  //equivaldria a un create

            modelBuilder.Entity<Region>()
                .HasRequired<Continent>(r => r.Continent)
                .WithMany(c => c.Regions)
                .HasForeignKey<int>(r => r.ContinentId);

            modelBuilder.Entity<Continent>()
                .HasOptional<Player>(c => c.Owner)
                .WithMany(p => p.Continents)
                .HasForeignKey(c => c.OwnerId);

            base.OnModelCreating(modelBuilder);
        }

        private void Setup()
        {
            RiskContext ctx = RiskContext.getInstance();
            //INICIAR CONTINENTE 1
            Continent c1 = new Continent() { Name = "Australia", Bonus = 5 };

            Region r1c1 = new Region() { Name = "Canguro", Troops = 5 };
            Region r2c1 = new Region() { Name = "Koala", Troops = 5 };
            Region r3c1 = new Region() { Name = "Ornitorrinco", Troops = 5 };

            c1.Regions.Add(r1c1);
            c1.Regions.Add(r2c1);
            c1.Regions.Add(r3c1);

            //INICIAR CONTINENTE 2
            Continent c2 = new Continent() { Name = "Baleares", Bonus = 7 };

            Region r1c2 = new Region() { Name = "Menorca", Troops = 5 };
            Region r2c2 = new Region() { Name = "Mallorca", Troops = 5 };
            Region r3c2 = new Region() { Name = "Tenerife", Troops = 5 };

            c2.Regions.Add(r1c2);
            c2.Regions.Add(r2c2);
            c2.Regions.Add(r3c2);

            //VECINDADES

            r1c1.Neighbours.Add(r1c2, r2c1);
            r2c1.Neighbours.Add(r1c1, r3c1);
            r3c1.Neighbours.Add(r2c1, r3c2);
            r1c2.Neighbours.Add(r1c1, r2c2);
            r2c2.Neighbours.Add(r1c2, r3c2);
            r3c2.Neighbours.Add(r2c2, r3c1);


            //INICIAR JUGADORES

            Player p1 = new Player() { Name = "Pepe", Id = 1, Order = 1 };
            p1.Regions.Add(r1c1);
            p1.Regions.Add(r1c2);
            p1.Regions.Add(r2c1);

            Player p2 = new Player() { Name = "Pedro", Id = 2, Order = 2 };
            p2.Regions.Add(r3c1);
            p2.Regions.Add(r2c2);
            p2.Regions.Add(r3c2);


            ctx.Continents.Add(c1);
            ctx.Continents.Add(c2);

            ctx.Players.Add(p1);
            ctx.Players.Add(p2);

            ctx.SaveChanges();
        }
    }
}
