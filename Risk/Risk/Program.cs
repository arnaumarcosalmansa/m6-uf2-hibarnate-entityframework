using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Risk.Tables;
using Risk.Contexts;

namespace Risk
{
    class Program
    {
        static void Main(string[] args)
        {
            using (RiskContext ctx = new RiskContext())
            {
                Game g = new Game();
                g.Play(ctx);
            }
        }
    }

	//muy optimizable
	class Game
	{
		private RiskContext ctx;

		public void Play(RiskContext ctx)
		{
			this.ctx = ctx;
			this.Setup();

            Player winner = null;
            bool finished = false;

			while(!finished)
			{
				IEnumerable<Player> players = new SortedSet<Player>(ctx.Players);
                players = players.Where((player) => { return player.Regions.Count > 0; });

                foreach (Player player in players)
				{
					View();
					Console.WriteLine("Turno de {0}", player.Name);
                    Console.WriteLine("DISTRIBUCIÓN DE TROPAS");
					SetTroops(player, player.Regions, player.Continents);
                    Console.WriteLine("ATAQUES");
					AttackPhase(player, player.Regions);
                    ChangeContinentOwnerIfNeeded(player);
                    Console.WriteLine("REESTRUCTURACIÓN");
                    RestructurePhase(player.Regions);
                    ctx.SaveChanges();
					Console.WriteLine("Final del turno de {0}", player.Name);

                    if(players.Where((p) => { return player.Regions.Count > 0; }).Count() == 1)
                    {
                        winner = player;
                        finished = true;
                        break;
                    }
                }
			}

			winner.Wins++;
            Console.WriteLine("¡Ha ganado {0}!", winner.Name);
			View();
			ctx.SaveChanges();
		}

		private void View()
		{
			Console.WriteLine("Regiones:");
			foreach(Region r in ctx.Regions)
			{
				Console.WriteLine("{0} - {1} - {2} - {3} - {4}", r.Id, r.Name, r.Continent.Name, r.Owner.Name, r.Troops);
			}

			Console.WriteLine("Jugadores:");
			foreach(Player p in ctx.Players)
			{
				Console.WriteLine("{0} - {1} - Continentes: {2}", p.Id, p.Name, p.Continents.Count());
			}
		}

		private void DisplayRegionsForAttack(Player player, SortedSet<Region> regions)
		{
			Console.WriteLine("[Id - NombreRegionJugador] => Tropas");
			Console.WriteLine("------[Id - NombreRegionEnemiga] => {Propietario tiene N tropas}");

			foreach (Region reg in regions)
			{
				Console.WriteLine("[{0} - {1}] => {2}", reg.Id, reg.Name, reg.Troops);
				IEnumerable<Region> neighbourEnemyRegions = reg.Neighbours.Where((region) => { return region.Owner != player; });
                if (neighbourEnemyRegions.Count() > 0)
                {
                    foreach (Region enemyReg in neighbourEnemyRegions)
                    {
                        Console.WriteLine("------[{0} - {1}] => [{2} tiene {3} tropas]", enemyReg.Id, enemyReg.Name, enemyReg.Owner.Name, enemyReg.Troops);
                    }
                }
                else
                {
                    Console.WriteLine("------Sin regiones vecinas enemigas.");
                }
			}
		}

		private void AttackPhase(Player player, SortedSet<Region> regions)
		{
			bool keepAttacking = true;

			Console.WriteLine("¿Quieres atacar?(S/N)");
			keepAttacking = Console.ReadLine().ToLower() == "s";

			while(keepAttacking)
			{
				DisplayRegionsForAttack(player, regions);
				Console.WriteLine("Selecciona desde que región vas a atacar:");
				int idAttackingRegion = Convert.ToInt32(Console.ReadLine());

				Func<Region, bool> regionById = (region) => { return region.Id == idAttackingRegion; };

				if (regions.Any(regionById))
				{
					Region attackingRegion = regions.First((region) => { return region.Id == idAttackingRegion; });

					Console.WriteLine("Selecciona a qué región vas a atacar:");
					int idAttackedRegion = Convert.ToInt32(Console.ReadLine());
					IEnumerable<Region> neighbourEnemyRegions = attackingRegion.Neighbours.Where((region) => { return region.Owner != player; });

					if (neighbourEnemyRegions.Any((region) => { return region.Id == idAttackedRegion; }))
					{
						Region attackedRegion = neighbourEnemyRegions.First((region) => { return region.Id == idAttackedRegion; });
						Attack(attackingRegion, attackedRegion);

                        ctx.SaveChanges();

						Console.WriteLine("Resultados del ataque.");
						DisplayRegionsForAttack(player, regions);

						Console.WriteLine("¿Seguir atacando?(S/N)");
                        string ans = Console.ReadLine();
                        keepAttacking = ans.ToLower() != "n" && !ctx.Regions.ToList().All(r => { return r.Owner == player; });
					}
				}
			}
		}

		private void Attack(Region attacking, Region attacked)
		{
			int attackingTroops = attacking.Troops > 3 ? 3 : attacking.Troops - 1;
			int defendingTroops = attacked.Troops > 2 ? 2 : attacked.Troops;

            if(attackingTroops == 0) { Console.WriteLine("No se puede atacar con 0 unidades."); return; }

			DiceRoll(attackingTroops, defendingTroops, out int attackerLosses, out int defenderLosses);

			attacking.Troops -= attackerLosses;
			attacked.Troops -= defenderLosses;

			bool conquest = attacked.Troops == 0;

			if(conquest)
			{
				attacked.Troops = attacking.Troops - 1;
				attacking.Troops = 1;
				attacked.Owner = attacking.Owner;
			}

            Console.WriteLine("Ataque realizado.");
		}

        private void ChangeContinentOwnerIfNeeded(Player p)
        {
            foreach(Continent c in ctx.Continents.ToList())
            {
                if(c.Regions.All(r => { return r.Owner == p; }))
                {
                    c.Owner = p;
                }
            }
        }

        private void RestructurePhase(SortedSet<Region> regions)
        {
            Console.WriteLine("Tus regiones:");
            foreach(Region r in regions)
            {
                Console.WriteLine("{0} - {1} -> {2} tropas", r.Id, r.Name, r.Troops);
            }

            Console.Write("Escoge la primera region: ");
            int rega = Convert.ToInt32(Console.ReadLine());
            Console.Write("Escoge la segunda region: ");
            int regb = Convert.ToInt32(Console.ReadLine());
            Console.Write("Escoge la cantidad de tropas a mover entre las regiones: ");
            int troopmov = Convert.ToInt32(Console.ReadLine());

            Region A = regions.First<Region>(r => { return r.Id == rega; });
            Region B = regions.First<Region>(r => { return r.Id == regb; });

			if(A.Troops - troopmov > 0)
			{
				A.Troops -= troopmov;
				B.Troops += troopmov;
			}
            
            ctx.SaveChanges();

            Console.WriteLine("Tropas movidas.");
        }


        private void Setup()
		{
            //INICIAR CONTINENTE 1
            Continent c1 = new Continent() { Name = "Australia", Bonus = 5 };

            Region r1c1 = new Region() { Name = "Canguro", Troops = 5 };
            Region r2c1 = new Region() { Name = "Koala", Troops = 5 };
            Region r3c1 = new Region() { Name = "Ornitorrinco", Troops = 5 };

            c1.Regions.Add(r1c1);
            c1.Regions.Add(r2c1);
            c1.Regions.Add(r3c1);

            //INICIAR CONTINENTE 2
            Continent c2 = new Continent() { Name = "Baleares", Bonus = 7 };

            Region r1c2 = new Region() { Name = "Menorca", Troops = 5 };
            Region r2c2 = new Region() { Name = "Mallorca", Troops = 5 };
            Region r3c2 = new Region() { Name = "Tenerife", Troops = 5 };

            c2.Regions.Add(r1c2);
            c2.Regions.Add(r2c2);
            c2.Regions.Add(r3c2);

            //VECINDADES

            r1c1.Neighbours.Add(r1c2, r2c1);
            r2c1.Neighbours.Add(r1c1, r3c1);
            r3c1.Neighbours.Add(r2c1, r3c2);
            r1c2.Neighbours.Add(r1c1, r2c2);
            r2c2.Neighbours.Add(r1c2, r3c2);
            r3c2.Neighbours.Add(r2c2, r3c1);


            //INICIAR JUGADORES

            Player p1 = new Player() { Name = "Pepe", Id = 1, Order = 1 };
            p1.Regions.Add(r1c1);
            p1.Regions.Add(r1c2);
            p1.Regions.Add(r2c1);

            Player p2 = new Player() { Name = "Pedro", Id = 2, Order = 2 };
            p2.Regions.Add(r3c1);
            p2.Regions.Add(r2c2);
            p2.Regions.Add(r3c2);


            ctx.Continents.Add(c1);
            ctx.Continents.Add(c2);

            ctx.Players.Add(p1);
            ctx.Players.Add(p2);

            ctx.SaveChanges();
		}

		Func<SortedSet<Region>, SortedSet<Continent>, int> calcTroops = (regions, continents) => { return 5 + regions.Count / 3 + continents.Sum((continent) => { return continent.Bonus; }); };

		private void SetTroops(Player player, SortedSet<Region> playerRegions, SortedSet<Continent> playerContinents)
		{
			int troopsToAdd = calcTroops(playerRegions, playerContinents);

			Console.WriteLine("El jugador {0} recibe {1} tropas.", player.Name, troopsToAdd);

			int addedTroops = 0;

			while(addedTroops != troopsToAdd)
			{
				Console.WriteLine("Te quedan {0} tropas. ¿Dónde las quieres distribuir?\nTus regiones són:", troopsToAdd - addedTroops);
				foreach(Region reg in playerRegions)
				{
					Console.WriteLine("[{0}] => {1}", reg.Id, reg.Name);
				}
				Console.WriteLine("Introduce una region:");
				int idSelectedRegion = Convert.ToInt32(Console.ReadLine());

				Func<Region, bool> regionById = (region) => { return region.Id == idSelectedRegion; };

				if (playerRegions.Any(regionById))
				{
					Region reg = playerRegions.First(regionById);
					Console.WriteLine("¿Cuantas tropas quieres destinar a {0}?", reg.Name);

					int troops = Convert.ToInt32(Console.ReadLine());

					if(troops <= (troopsToAdd - addedTroops))
					{
						reg.Troops += troops;
						addedTroops += troops;
					}
				}
			}
			ctx.SaveChanges();
			Console.WriteLine("Ya has colocado todas tus tropas.");
		}

		public static void DiceRoll(int natk, int ndef, out int lossatk, out int lossdef)
		{
			int[] atk = new int[natk];
			int[] def = new int[ndef];
			lossatk = 0;
			lossdef = 0;

			Random r = new Random();
			for (int i = 0; i < natk; i++)
			{
				atk[i] = r.Next(1, 7);
			}
			for (int i = 0; i < ndef; i++)
			{
				def[i] = r.Next(1, 7);
			}
			Array.Sort(atk);
			Array.Reverse(atk);
			Array.Sort(def);
			Array.Reverse(def);

			for (int i = 0; i < ndef && i < natk; i++)
			{
				if (atk[i] > def[i]) lossdef++;
				else lossatk++;
			}
		}
	}
}

