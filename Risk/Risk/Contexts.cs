﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Risk.Tables;

namespace Risk.Contexts
{
    class RiskContext : DbContext
    {
        public RiskContext() : base("Risk")
        {
            Console.WriteLine("Creando contexto... puede tardar un poco.");
        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Continent> Continents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists); //update, pero crea la DB!
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RiskContext>());  //aquest seria un metode update pero si canvies el model (els POCOS) reinicia la DB
            Database.SetInitializer(new DropCreateDatabaseAlways<RiskContext>());  //equivaldria a un create

            modelBuilder.Entity<Region>()
                .HasRequired<Continent>(r => r.Continent)
                .WithMany(c => c.Regions)
                .HasForeignKey<int>(r => r.ContinentId);

            modelBuilder.Entity<Continent>()
                .HasOptional<Player>(c => c.Owner)
                .WithMany(p => p.Continents)
                .HasForeignKey(c => c.OwnerId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
