﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Risk
{
    static class Extensions
    {
        public static void Add<T>(this SortedSet<T> set, params T[] add)
        {
            foreach(T item in add)
            {
                set.Add(item);
            }
        }
    }
}
