DROP DATABASE clubNautic;
CREATE DATABASE clubNautic;

USE clubNautic;

CREATE TABLE Address
(
	id BIGINT,
	country VARCHAR(255) NOT NULL,
	ca VARCHAR(255) NOT NULL,
	city VARCHAR(255) NOT NULL,
	zipcode VARCHAR(255) NOT NULL,
	street VARCHAR(255) NOT NULL,
	number INT,
	letter CHAR,
	floor INT,
	door INT,
	PRIMARY KEY (id)
);

CREATE TABLE Persona
(
	dni VARCHAR(9),
	name VARCHAR(20) NOT NULL,
	surname VARCHAR(40) NOT NULL,
	birthDate DATE NOT NULL,
	gender CHAR NOT NULL,
	address BIGINT,
	FOREIGN KEY (address) REFERENCES Address(id),
	PRIMARY KEY (dni)
);

CREATE TABLE Usuario
(
	dniPersona VARCHAR(9),
	type INT NOT NULL,
	observations VARCHAR(6000),
	PRIMARY KEY (dniPersona),
	FOREIGN KEY (dniPersona) REFERENCES Persona(dni)
);

CREATE TABLE Monitor
(
	dniPersona VARCHAR(9),
	salary DOUBLE,
	PRIMARY KEY (dniPersona),
	FOREIGN KEY (dniPersona) REFERENCES Persona(dni)
);

CREATE TABLE Activitat
(
	idActivity VARCHAR(16),
	name VARCHAR(255) NOT NULL,
	type int,
	price DOUBLE,
	startDate DATE NOT NULL,
	endDate DATE NOT NULL,
	PRIMARY KEY (idActivity)
);

CREATE TABLE Inscripcio
(
	idActivity VARCHAR(16),
	dniPersona VARCHAR(9),
	PRIMARY KEY (idActivity, dniPersona),
	FOREIGN KEY (idActivity) REFERENCES Activitat(idActivity),
	FOREIGN KEY (dniPersona) REFERENCES Persona(dni)
);

