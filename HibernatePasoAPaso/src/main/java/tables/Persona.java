package tables;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@Inheritance(strategy = InheritanceType.JOINED)
public class Persona
{
	@Id
	@Column(name = "dni", nullable = false)
	private String DNI;
	@Column(name = "name", nullable = false)
	private String nombre;
	@Column(name = "surname", nullable = false)
	private String apellidos;
	@Column(name = "birthDate", nullable = false)
	private Date fechaNacimiento;
	@Column(name = "gender")
	private char sexo;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="address")
	private Direccion direccion;
	
	
	public String getDNI() {
		return DNI;
	}
	public void setDNI(String dNI) {
		DNI = dNI;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	
	
}
