package tables;

import javax.persistence.Column;
import javax.persistence.Id;

public class Direccion
{
	@Id
	@Column(name="id")
	private long id;
	@Column(name="country")
	private String pais;
	@Column(name="ca")
	private String ca;
	@Column(name="city")
	private String ciudad;
	@Column(name="zipcode")
	private String coodigoPostal;
	@Column(name="street")
	private String calle;
	@Column(name="number")
	private int numero;
	@Column(name="letter")
	private char letra;
	@Column(name="floor")
	private int floor;
	@Column(name="door")
	private int door;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCa() {
		return ca;
	}
	public void setCa(String ca) {
		this.ca = ca;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getCoodigoPostal() {
		return coodigoPostal;
	}
	public void setCoodigoPostal(String coodigoPostal) {
		this.coodigoPostal = coodigoPostal;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public char getLetra() {
		return letra;
	}
	public void setLetra(char letra) {
		this.letra = letra;
	}
	public int getFloor() {
		return floor;
	}
	public void setFloor(int floor) {
		this.floor = floor;
	}
	public int getDoor() {
		return door;
	}
	public void setDoor(int door) {
		this.door = door;
	}
	
	
}
