package logic;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import tables.Persona;

public class PersonFactory
{
	public <T> Persona create(Class<T> classe) throws InstantiationException, IllegalAccessException
	{
		T result = null;
		
		if(!classe.getSuperclass().equals(Persona.class) || !classe.equals(Persona.class))
		{
			throw new IllegalArgumentException("Clase no valida.");
		}
		
		result = (T) classe.newInstance();
		
		
		
		return Persona.class.cast(result);
	}
	
	private <T> void fill(T persona) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		Method[] metodos = persona.getClass().getMethods();
		
		for(Method m : metodos)
		{
			if(m.getName().startsWith("set"))
			{
				Class[] parameterTypes = m.getParameterTypes();
				Object[] parameters = new Object[parameterTypes.length]; 
				
				for(int i = 0; i < parameterTypes.length; i++)
				{
					generateRandomFromType(parameterTypes[i]);
				}
				
				m.invoke(persona, parameters);
			}
		}
	}
	
	private <T> Object generateRandomFromType(Class<T> classe)
	{
		Object result = null;
		
		if(classe.equals(String.class))
		{
			result = new String();
		}
		
		return result;
	}
}
